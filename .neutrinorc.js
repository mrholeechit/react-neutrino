module.exports = {
  use: [
    '@neutrinojs/airbnb',
    [
      '@neutrinojs/react',
      {
        html: {
          title: 'react-neutrino'
        },
        style: [{
          test: /\.scss$/,
          modulesTest: /\.module\.scss$/,
          loaders: [{
            loader: require.resolve('sass-loader'),
            useId: 'sass'

          }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
          }]
        }, {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        }]
      }
    ],
    '@neutrinojs/jest',
    ['@neutrinojs/style-loader', {
      loaders: [{
        loader: 'sass-loader',
        useId: 'sass',
      }],
      test: /\.scss$/,
    }]
  ],
  options: {
    source: 'js/src',
    test: 'js/test',
    output: 'src/main/webapp'
  }
};
