import { connect } from 'react-redux';
import { string, func } from 'prop-types';
import { createSelector, createStructuredSelector } from 'reselect';

const AppPage = ({
  data: { data },
  onClick,
}) => (
  <div className="App">
    <button className="uni-btns" onClick={onClick}>{data}</button>
    <h1>Welcome to {data}</h1>
  </div>
);

AppPage.propTypes = {
  data: string.isRequired,
  onClick: func.isRequired,
};


const makeSelectSearch = createStructuredSelector({
  data: createSelector(
    state => state.data,
    state => state,
  ),
});

const dispatchToProps = dispatch => ({
  onClick: () => dispatch({
    type: 'CHANGE_NAME',
    value: 'felipe fernandes diogo',
  }),
});


export default connect(makeSelectSearch, dispatchToProps)(AppPage);
