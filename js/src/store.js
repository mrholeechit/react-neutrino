import { createStore } from 'redux';
import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import homeReducer from './modules/home';

export default function configureStore(initialState = {}) {
  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  // const composeEnhancers =
  //   process.env.NODE_ENV !== 'production' &&
  //     typeof window === 'object' &&
  //     window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
  //     window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
  /* eslint-enable */

  const store = createStore(
    combineReducers({
      home: homeReducer,
    }),
    fromJS(initialState),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // eslint-disable-line
  );

  return store;
}
