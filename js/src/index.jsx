import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import HomePage from './containers/HomePage';
import configureStore from './store';

import './public/styles/css/styles.css';

const store = configureStore();

const root = document.getElementById('root');
const load = () => render(
  (
    <Provider store={store}>
      <AppContainer>
        <HomePage />
      </AppContainer>
    </Provider>
  ), root,
);

// This is needed for Hot Module Replacement
if (module.hot) {
  module.hot.accept('./containers/HomePage', load);
}

load();
