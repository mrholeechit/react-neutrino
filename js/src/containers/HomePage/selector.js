import { createSelector } from 'reselect';

export default () => {
  console.log('state'); // eslint-disable-line
  return createSelector(
    state => state.get('home'),
    state => state.toJS(),
  );
};
