
import React from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import homeSelect from './selector';

const HomePage = ({
  data: {
    tag,
  },
  onChange,
  onSearch,
}) => (
  <div>
    <input id="tag" onChange={onChange} />
    <button type="submit" onClick={onSearch}>
      Add Todo
    </button>
    <span>{tag}</span>
  </div>
);

const mapStateToProps = createStructuredSelector({
  data: homeSelect(),
});

function mapDispatchToProps(dispatch) {
  return {
    onSearch: (tag) => {
      console.log(tag);
      dispatch(tag);
    },
    onChange: ({ target: { value } }) => {
      console.log(`asdasd ${value}`);
    },
  };
}

HomePage.propTypes = {
  data: object.isRequired, // eslint-disable-line
  onChange: func.isRequired,
  onSearch: func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
