import { fromJS } from 'immutable';

const ON_SEARCH = 'HOME_PAGE/ON_SEARCH';

export const onSearch = tag => ({
  type: ON_SEARCH,
  tag,
});

const initialState = fromJS({
  tag: '',
  tags: [],
});

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ON_SEARCH: return state.set('tag', action.tag);
    default: return state;
  }
};
